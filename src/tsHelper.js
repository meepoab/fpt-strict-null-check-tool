// @ts-check
const path = require('path');
const ts = require('typescript');
const fs = require('fs');

module.exports.getImportsForFile = function getImportsForFile(file, srcRoot) {
    const fileInfo = ts.preProcessFile(fs.readFileSync(file).toString());

    if (file.endsWith('/main.ts') ||
        file.endsWith('/test.ts')) {
        return [];
    }

    return fileInfo.importedFiles
        .map(importedFile => importedFile.fileName)
        .filter(fileName => !/^vs\/css!/.test(fileName)) // remove css imports
        .filter(x => /\//.test(x)) // remove node modules (the import must contain '/')
        .filter(x => !x.startsWith('@')) // remove angular
        .filter(x => !x.startsWith('rxjs')) // remove rxjs
        .filter(x => !x.startsWith('froala')) // remove froala
        .map(fileName => {
            if (/(^\.\/)|(^\.\.\/)/.test(fileName)) {
                return path.join(path.dirname(file), fileName);
            }
            if (/src/.test(fileName)) {
                return path.join(srcRoot, fileName.substring(3));
            }
            return fileName;
        }).map(fileName => {
            if (fs.existsSync(`${fileName}.ts`)) {
                return `${fileName}.ts`;
            }
            if (fs.existsSync(`${fileName}.js`)) {
                return `${fileName}.js`;
            }
            if (fs.existsSync(`${fileName}.d.ts`)) {
                return `${fileName}.d.ts`;
            }
            throw new Error(`Unresolved import ${fileName} in ${file}`);
        });
};
